var express = require('express');

var app = express();
var bcrypt = require('bcryptjs');
var middlewareAuth = require('../middlewares/auth');
var Usuario = require('../models/usuario');
// routes
app.get('/', (req, res, next) => {
    var from = req.query.from || 0;
    from = Number(from);
    Usuario.find({},'nombre email img role google')
    .skip(from)
    .limit(15)
    .exec((err, usuarios) => {
        if (err) {
            return res.status(500).json({
                ok: true,
                mensaje: 'Error cargando usuarios',
                errors: err
            });
        }

        Usuario.count({}, (err, count) => {
            res.status(200).json({
                ok: true,
                count: count,
                usuarios: usuarios
            });
         });

        
    });
    

});

app.put('/:id', [middlewareAuth.verifyToken, middlewareAuth.sameUserOrAdmin], (req, res, next) => {
    var id = req.params.id;


    console.log(req);

    Usuario.findById(id, (err, usuario) => {
        var body = req.body;    
        if (err) {
            return res.status(500).json({
                ok: true,
                mensaje: 'Error buscar usuarios',
                errors: err
            });
        }

        if (!usuario) {
            return res.status(400).json({
                ok: true,
                mensaje: 'Error buscar usuarios',
                errors: err
            });
        }

        usuario.nombre = body.nombre;
        usuario.email = body.email;
        usuario.role = body.role;

        usuario.save((err, usuarioGuardado) => {
            if (err) {
                return res.status(400).json({
                    ok: true,
                    mensaje: 'Error update usuarios',
                    errors: err
                });
            }
    
            res.status(200).json({
                ok: true,
                usuario: usuarioGuardado 
            });
        })

    });
});

//app.post('/', middlewareAuth.verifyToken, (req, res, next) => {
app.post('/', (req, res, next) => {

    console.log('req', req.body);
    var body = req.body;

    var usuario = new Usuario({
        nombre: body.nombre,
        email: body.email,
        password: bcrypt.hashSync(body.password, 10),
        img: body.img,
        role: body.role
    });

    usuario.save((err, usuarioGuardado) => {
        if (err) {
            return res.status(400).json({
                ok: true,
                mensaje: 'Error save usuarios',
                errors: err
            });
        }

        res.status(201).json({
            ok: true,
            usuario: usuarioGuardado 
        });

    });
});

app.delete('/:id', [middlewareAuth.verifyToken, middlewareAuth.verifyAdmin], (req, res) => {
    var id = req.params.id;

    Usuario.findByIdAndRemove(id, (err, result) => {
        if (err) {
            return res.status(500).json({
                ok: true,
                mensaje: 'Error delete usuarios',
                errors: err
            });
        }

        res.status(200).json({
            ok: true,
            usuario: result 
        });
    })
})

module.exports = app;
