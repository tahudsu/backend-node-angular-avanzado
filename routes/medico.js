var express = require('express');

var app = express();
var bcrypt = require('bcryptjs');
var middlewareAuth = require('../middlewares/auth');

var Medico = require('../models/medico');

// routes
app.get('/', (req, res, next) => {
    var from = req.query.from || 0;
    from = Number(from);

    Medico.find({},'nombre img usuario hospital')
    .skip(from)
    .limit(5)
    .populate('usuario', 'nombre email')
    .populate('hospital')
    .exec((err, medico) => {
        if (err) {
            return res.status(500).json({
                ok: true,
                mensaje: 'Error cargando medico',
                errors: err
            });
        }

        Medico.count({}, (err, count) => {
            res.status(200).json({
                ok: true,
                medico: medico,
                count: count
            });
        });
    });
});

app.get('/:id', (req, res, next) => {
    const id = req.params.id;

    Medico.findById(id)
        .populate('usuario', 'nombre email img')
        .populate('hospital')
        .exec((err, medico) => {
            if (err) {
                return res.status(500).json({
                    ok: true,
                    mensaje: 'Error buscar medico',
                    errors: err
                });
            }
    
            if (!medico) {
                return res.status(400).json({
                    ok: true,
                    mensaje: 'Error buscar medico',
                    errors: err
                });
            }

            return res.status(200).json({
                ok: true,
                medico: medico
            });
        });

})

// app.put('/:id', middlewareAuth.verifyToken, (req, res, next) => {
    app.put('/:id', (req, res, next) => {

        var id = req.params.id;
    
    
        console.log(req);
    
        Medico.findById(id, (err, medico) => {
            var body = req.body;    
            if (err) {
                return res.status(500).json({
                    ok: true,
                    mensaje: 'Error buscar medico',
                    errors: err
                });
            }
    
            if (!medico) {
                return res.status(400).json({
                    ok: true,
                    mensaje: 'Error buscar medico',
                    errors: err
                });
            }
    
            medico.nombre = body.nombre,
            medico.usuario = body.usuario_id,
            medico.hospital = body.hospital_id
    
            medico.save((err, medicoSaved) => {
                if (err) {
                    return res.status(400).json({
                        ok: true,
                        mensaje: 'Error update medico',
                        errors: err
                    });
                }
        
                res.status(200).json({
                    ok: true,
                    medico: medicoSaved 
                });
            })
    
        });
    });
// app.post('/', middlewareAuth.verifyToken, (req, res, next) => {
app.post('/', (req, res, next) => {

        console.log('req', req.body);
        var body = req.body;
    
        var medico = new Medico({
            nombre: body.medico.nombre,
            usuario: body.usuario._id, 
            hospital: body.medico.hospital
        });
    
        medico.save((err, medicoSaved) => {
            if (err) {
                return res.status(400).json({
                    ok: true,
                    mensaje: 'Error save medico',
                    errors: err
                });
            }
    
            res.status(201).json({
                ok: true,
                medico: medicoSaved 
            });
    
        });
    });

    app.delete('/:id', middlewareAuth.verifyToken, (req, res) => {
        var id = req.params.id;
    
        Medico.findByIdAndRemove(id, (err, result) => {
            if (err) {
                return res.status(500).json({
                    ok: true,
                    mensaje: 'Error delete medico',
                    errors: err
                });
            }
    
            res.status(200).json({
                ok: true,
                medico: result 
            });
        })
    })

module.exports = app;
