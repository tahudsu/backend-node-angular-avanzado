var express = require('express');

var app = express();

var fileUpload = require('express-fileupload');
var fs = require('fs');
app.use(fileUpload());

var Usuario = require('../models/usuario');
var Medico = require('../models/medico');
var Hospital = require('../models/hospital');


// routes
app.put('/:tipo/:id', (req, res, next) => {
  console.log(req.files);
  var tipo = req.params.tipo;
  var id = req.params.id;

  var tiposValidos = ['hospitales', 'medicos', 'usuarios'];

  if (tiposValidos.indexOf(tipo) < 0) {
    return res.status(400).json({
      ok: false,
      mensaje: 'tipo not allowed',
      errors: { message: 'las tipos valida son ' + tiposValidos.join(', ') }
    });
  }
  if (!req.files) {
    return res.status(400).json({
      ok: false,
      mensaje: 'no file added',
      errors: { message: 'no file added' }
    });
  }
  var archivo = req.files.imagen;
  var nombreCortado = archivo.name.split('.');
  var extensionArchivo = nombreCortado[nombreCortado.length - 1];

  // Solo estas extensiones seran aceptadas
  var extAllowed = ['png', 'jpg', 'gif', 'jpeg'];
  if (extAllowed.indexOf(extensionArchivo) < 0) {
    return res.status(400).json({
      ok: false,
      mensaje: 'file extension not allowed',
      errors: { message: 'las extensiones valida son ' + extAllowed.join(', ') }
    });
  }

  // nombre de archivo personalizado
  var nombreArchivo = `${id}-${new Date().getMilliseconds()}.${extensionArchivo}`;
  // mover del temp a un path, el archivo
  var path = `./uploads/${tipo}/${nombreArchivo}`;

  archivo.mv(path, err => {
    if (err) {
      return res.status(500).json({
        ok: false,
        mensaje: 'error al mover el archivo',
        errors: { message: err }
      });
    }

    subirPorTipo(tipo, id, nombreArchivo, res);
  });
  /* res.status(200).json({
    ok: true,
    mensaje: 'peticion realizada correctamente'
  }); */

});

function subirPorTipo(tipo, id, nombreArchivo, res) {
  if (tipo === 'usuarios') {
    Usuario.findById(id, (err, usuario) => {
      if (err) {
        // err
      }

      if (!usuario) {
        return res.status(400).json({
          ok: true,
          mensaje: 'Usuario no existe',
          errors: { message: 'usuario no existe'}
        });
      }
      console.log(usuario);
      var pathViejo = './uploads/usuarios/' + usuario.img;
      console.log(pathViejo);
      if (fs.existsSync(pathViejo) && usuario.img) {
        fs.unlink(pathViejo, err => {
          if (err) {
            console.log(err);
            throw err;
          }
        });
      }

      usuario.img = nombreArchivo;
      usuario.save((err, usuarioActualizado) => {
        return res.status(200).json({
          ok: true,
          mensaje: 'Imagen actualizada',
          usuario: usuarioActualizado
        });
      });
    })
  }

  if (tipo === 'medicos') {
    Medico.findById(id, (err, medico) => {
      if (err) {

      }

      if (!medico) {
        return res.status(400).json({
          ok: true,
          mensaje: 'medico no existe',
          errors: { message: 'medico no existe'}
        });
      }

      var pathViejo = './uploads/medico/' + medico.img;
      if (fs.existsSync(pathViejo) && medico.img) {
        fs.unlink(pathViejo);
      }

      medico.img = nombreArchivo;
      medico.save((err, medicoActualizado) => {
        return res.status(200).json({
          ok: true,
          mensaje: 'Imagen actualizada',
          medico: medicoActualizado
        });
      });
    });
  }
  if (tipo === 'hospitales') {
    Hospital.findById(id, (err, hospital) => {
      if (err) {

      }

      if (!hospital) {
        return res.status(400).json({
          ok: true,
          mensaje: 'hospital no existe',
          errors: { message: 'hospital no existe'}
        });
      }

      var pathViejo = './uploads/hospital/' + hospital.img;
      if (fs.existsSync(pathViejo) && hospital.img) {
        fs.unlink(pathViejo);
      }

      hospital.img = nombreArchivo;
      hospital.save((err, hospitalActualizado) => {
        return res.status(200).json({
          ok: true,
          mensaje: 'Imagen actualizada',
          hospital: hospitalActualizado
        });
      });
    });
  }
}

module.exports = app;
