var express = require('express');

var app = express();
var bcrypt = require('bcryptjs');
var middlewareAuth = require('../middlewares/auth');

var Hospital = require('../models/hospital');

// routes
app.get('/', (req, res, next) => {
    Hospital.find({})
        .populate('usuario', 'nombre email')
        .exec((err, hospital) => {
            if (err) {
                return res.status(500).json({
                    ok: true,
                    mensaje: 'Error cargando hospital',
                    errors: err
                });
            }
            Hospital.count({}, (err, count) => {
                res.status(200).json({
                    ok: true,
                    hospital: hospital,
                    count: count,
                });
            });

        });
});

// app.put('/:id', middlewareAuth.verifyToken, (req, res, next) => {
app.put('/:id', (req, res, next) => {

    var id = req.params.id;


    console.log(req);

    Hospital.findById(id, (err, hospital) => {
        var body = req.body;
        if (err) {
            return res.status(500).json({
                ok: true,
                mensaje: 'Error buscar hospital',
                errors: err
            });
        }

        if (!hospital) {
            return res.status(400).json({
                ok: true,
                mensaje: 'Error buscar hospital',
                errors: err
            });
        }

        hospital.nombre = body.nombre;
        hospital.usuario = body.usuario_id;
        /* usuario.email = body.email;
        usuario.role = body.role; */

        hospital.save((err, hospitalSaved) => {
            if (err) {
                return res.status(400).json({
                    ok: true,
                    mensaje: 'Error update hospital',
                    errors: err
                });
            }

            res.status(200).json({
                ok: true,
                hospital: hospitalSaved
            });
        })

    });
});

// app.post('/', middlewareAuth.verifyToken, (req, res, next) => {
app.post('/', (req, res, next) => {

    console.log('req', req.body);
    var body = req.body;

    var hospital = new Hospital({
        nombre: body.nombre,
        usuario: req.body.usuario._id
    });

    hospital.save((err, hospitalSaved) => {
        if (err) {
            return res.status(400).json({
                ok: true,
                mensaje: 'Error save hospital',
                errors: err
            });
        }

        res.status(201).json({
            ok: true,
            hospital: hospitalSaved
        });

    });
});

app.delete('/:id', middlewareAuth.verifyToken, (req, res) => {
    var id = req.params.id;

    Hospital.findByIdAndRemove(id, (err, result) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                mensaje: 'Error delete hospital',
                errors: err
            });
        }

        res.status(200).json({
            ok: true,
            hospital: result
        });
    })
})

// app.get('/:id', middlewareAuth.verifyToken, (req, res) => {
app.get('/:id', (req, res) => {

    var id = req.params.id;

    Hospital.findById(id)
        .populate('usuario', 'nombre img email')
        .exec((err, hospital) => {
            if (err) {
                return res.status(500), json({
                    ok: false,
                    mensaje: '',
                    errors: err
                });
            }

            if (!hospital) {
                return res.status(400).json({
                    ok: false,
                    mensaje: 'el hospital con el id ' + id + 'no existe',
                    err: 'No existe un hospital con ese id'
                });
            }

            res.status(200).json({
                ok: true,
                hospital: hospital
            });
        });
});

module.exports = app;
