var express = require('express');

var app = express();
var bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');

var SEED = require('../config/config').SEED;

var Usuario = require('../models/usuario');

var CLIENT_ID = require('../config/config').CLIENT_ID;
var mdAuthenticacion = require('../middlewares/auth');
const { OAuth2Client } = require('google-auth-library');
const client = new OAuth2Client(CLIENT_ID);
// https://developers.google.com/identity/sign-in/web/backend-auth
app.post('/google', async (req, res) => {
    var token = req.body.token;

    var googleUser = await verify(token)
        .catch(e => {
            console.log(e);
            return res.status(403).json({
                ok: false,
                mensaje: 'Token no valido'
            });
        });

    Usuario.findOne({ email: googleUser.email }, (err, usuarioDB) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                mensaje: 'Error login usuarios',
                errors: err
            });
        }

        if (usuarioDB) {
            if (!usuarioDB.google) {
                return res.status(400).json({
                    ok: false,
                    mensaje: 'debe usar su auth normal'
                });
            } else {
                var token = jwt.sign({ usuario: usuarioDB }, SEED, { expiresIn: 14400 });

                res.status(200).json({
                    ok: true,
                    message: 'login ok',
                    token: token,
                    id: usuarioDB._id,
                    body: usuarioDB,
                    menu: obtenerMenu(usuarioDB.role)
                });
            }
        } else {
            // user dosent exist, save it
            var usuario = new Usuario();

            usuario.nombre = googleUser.nombre;
            usuario.email = googleUser.email;
            usuario.img = googleUser.img;
            usuario.google = true;
            usuario.password = ':)';

            usuario.save((err, usuarioDB) => {
                var token = jwt.sign({ usuario: usuarioDB }, SEED, { expiresIn: 14400 });
                console.log(usuarioDB);
                res.status(200).json({
                    ok: true,
                    message: 'login ok',
                    token: token,
                    id: usuarioDB._id,
                    body: usuarioDB,
                    menu: obtenerMenu(usuarioDB.role)
                });
            });
        }
    });

    /* return res.status(200).json({
        ok: true,
        mensaje: 'ok',
        googleUser: googleUser
    }); */
});

async function verify(token) {
    const ticket = await client.verifyIdToken({
        idToken: token,
        audience: CLIENT_ID,  // Specify the CLIENT_ID of the app that accesses the backend
        // Or, if multiple clients access the backend:
        //[CLIENT_ID_1, CLIENT_ID_2, CLIENT_ID_3]
    });

    const payload = ticket.getPayload();
    const userid = payload['sub'];
    // If request specified a G Suite domain:
    //const domain = payload['hd'];

    return {
        nombre: payload.name,
        email: payload.email,
        img: payload.picture,
        google: true,
    };
}



app.post('', (req, res) => {
    var body = req.body;
    Usuario.findOne({ email: body.email }, (err, usuarioDB) => {

        if (err) {
            return res.status(500).json({
                ok: false,
                mensaje: 'Error login usuarios',
                errors: err
            });
        }

        if (!usuarioDB) {
            return res.status(400).json({
                ok: false,
                mensaje: 'credenciales invalidas - email',
                errors: err
            });
        }

        if (!bcrypt.compareSync(body.password, usuarioDB.password)) {
            return res.status(400).json({
                ok: false,
                mensaje: 'credenciales invalidas - pass',
                errors: err
            });
        }

        //crean un token
        usuarioDB.password = ':)';
        var token = jwt.sign({ usuario: usuarioDB }, SEED, { expiresIn: 14400 });

        res.status(200).json({
            ok: true,
            message: 'login ok',
            token: token,
            id: usuarioDB._id,
            body: usuarioDB,
            menu: obtenerMenu(usuarioDB.role)
        });
    });

})

function obtenerMenu(ROLE) {
    var menu = [
        {
            titulo: 'Principal',
            icono: 'mdi mdi-gauge',
            submenu: [
                {
                    titulo: 'Dashboard',
                    url: '/dashboard'
                },
                {
                    titulo: 'ProgressBar',
                    url: '/progress'
                },
                {
                    titulo: 'Graficas',
                    url: '/graficas1'
                },
                {
                    titulo: 'Promesas',
                    url: '/promesas'
                },
                {
                    titulo: 'Rxjs',
                    url: '/rxjs'
                }
            ]
        },
        {
            titulo: 'Mantenimientos',
            icono: 'mdi mdi-folder-lock-open',
            submenu: [
                // { titulo: 'Usuarios', url: '/usuarios' },
                { titulo: 'Hospitales', url: '/hospitales' },
                { titulo: 'Médicos', url: '/medicos' }
            ]
        }
    ];

    if (ROLE === 'ADMIN_ROLE') {
        menu[1].submenu.unshift({ titulo: 'Usuarios', url: '/usuarios' });
    }

    return menu;
}

app.get('/renewToken', mdAuthenticacion.verifyToken, (req, res) => {
    // expires in 4 hours
    var token = jwt.sign({ usuario: req.usuario }, SEED, { expiresIn: 1440 });

    res.status(200).json({
        ok: true,
        usuario: req.usuario,
        token: token
    });
});

module.exports = app;
