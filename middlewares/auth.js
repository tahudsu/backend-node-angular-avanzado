var jwt = require('jsonwebtoken');

var SEED = require('../config/config').SEED;

// middleware token
exports.verifyToken = function(req, res, next) {
    var token = req.query.token;
    jwt.verify(token, SEED, (err, decoded) => {
        if (err) {
            return res.status(401).json({
                ok: true,
                mensaje: 'Error token',
                errors: err
            });
        }

        req.usuario = decoded.usuario;
        next();
    });
}

exports.verifyAdmin = function(req, res, next) {
    var usuario = req.usuario;

    if (usuario.role === 'ADMIN_ROLE') {
        next();
        return;
    } else {
        return res.status(401).json({
            ok: true,
            mensaje: 'Error token invalid',
            errors:  { message: 'No puede acceder' }
        });
    }
}

exports.sameUserOrAdmin = function(req, res, next) {
    var usuario = req.usuario;
    var id = req.params.id;

    if (usuario.role === 'ADMIN_ROLE' || usuario._id === id) {
        next();
        return;
    } else {
        return res.status(401).json({
            ok: true,
            mensaje: 'Error token invalid',
            errors:  { message: 'No puede acceder' }
        });
    }
}
