var moongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');

var Schema = moongoose.Schema;

var rolesValidos = {
    values: ['ADMIN_ROLE', 'USER_ROLE'],
    message: '{VALUE} no es un rol valido'
};

var usuarioSchema = new Schema({
    nombre: { type: String , required: [true, 'El nombre es necesario'] },
    email: { type: String , unique: true,  required: [true, 'El email es necesario'] },
    password: { type: String , required: [true, 'El password es necesario'] },
    img: { type: String , required: [false, 'El email es necesario'] },
    role: { type: String , required: [true, 'El role es necesario'], default: 'USER_ROLE', enum: rolesValidos },
    google: { type: Boolean, default: false }
});

usuarioSchema.plugin(uniqueValidator, { message: '{PATH} debe ser único'});
module.exports = moongoose.model('Usuario', usuarioSchema);
